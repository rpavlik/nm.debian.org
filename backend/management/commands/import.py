from django.core.management.base import BaseCommand
import sys
import logging
import json
from backend.export import load_db

log = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Import a JSON database dump'

    def add_arguments(self, parser):
        parser.add_argument("file", nargs="+", type=str, help="JSON dump file name")

        parser.add_argument("--quiet", action="store_true", dest="quiet", default=None,
                            help="Disable progress reporting")

    def handle(self, *args, **opts):
        FORMAT = "%(asctime)-15s %(levelname)s %(message)s"
        if opts["quiet"]:
            logging.basicConfig(level=logging.WARNING, stream=sys.stderr, format=FORMAT)
        else:
            logging.basicConfig(level=logging.INFO, stream=sys.stderr, format=FORMAT)

        for fname in opts["file"]:
            with open(fname) as fd:
                loaded = load_db(json.load(fd))
                loaded.save()
                log.info("%s: imported information about %d people", fname, len(loaded.people))

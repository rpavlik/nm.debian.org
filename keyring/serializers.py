from rest_framework import serializers
from . import models


class KeyExportSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Key
        exclude = ["id"]

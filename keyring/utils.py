import re


def rfc3156(text):
    """
    If the statement is an email, parse it as rfc3156, else return None
    """
    # https://tools.ietf.org/html/rfc4880#section-7
    if text.strip().startswith("-----BEGIN PGP SIGNED MESSAGE-----"):
        return None

    from keyring.openpgp import RFC3156
    res = RFC3156(text.encode("utf-8"))
    if not res.parsed:
        return None
    return res


def cleaned_text(text):
    msg = rfc3156(text)
    if msg is None:
        return re.sub(
                r".*?-----BEGIN PGP SIGNED MESSAGE-----.*?\r\n\r\n(.+?)-----BEGIN PGP SIGNATURE-----.+", r"\1",
                text, flags=re.DOTALL)
    else:
        if not msg.parsed:
            return text
        else:
            return msg.text.get_payload(decode=True)

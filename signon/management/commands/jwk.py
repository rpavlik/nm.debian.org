from __future__ import annotations
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
import logging
from jwcrypto import jwk

log = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "import Salsa user information"

    def add_arguments(self, parser):
        parser.add_argument('--generate', action="store_true", help="generate a new key pair")
        parser.add_argument('--public', action="store_true", help="output the public key")

    def handle(self, *args, **opts):
        if opts["generate"]:
            key = jwk.JWK.generate(kty='RSA', size=4096)
            print(key.export_private())
        elif opts["public"]:
            key = jwk.JWK.from_json(settings.SIGNON_KEY)
            print(key.export_public())
